import React from 'react';
import './Courses.css';
import {Button } from 'reactstrap';


const Course=({CourseNumber, CourseTitle})=>{
    const classStr=`Course Course-Number-${CourseNumber}  shrink shrink:hover`;
    return (
        <div className={classStr}>
            <h4 className="Course-Name limit">{CourseTitle}</h4>
            <Button className="Course-Button" color="success" >Course</Button>

        </div>
    )
}
export default Course;