import React from'react';
import './Courses.css';
import {Jumbotron} from 'reactstrap'
import Course from './Course';

const AvailableCourses=[
    {
        'CourseTitle':'New Operations Personnel Training'
    },
    {
        'CourseTitle':'Flight Operations Training'
    },
    {
        'CourseTitle':'Centralized Aviation Flight Records System (CAFRS)'
    },
    {
        'CourseTitle':'Individual Flight Record Folder Training'
    },
    {
        'CourseTitle':'Communications Training'
    },
    {
        'CourseTitle':'S3 Operations Training'
    }
]


const Courses= ()=>{
    return (
        <div className="Course-Offerings">
            {AvailableCourses.map((course, index)=>{
             return (<Course CourseNumber={index} CourseTitle={course.CourseTitle}/>)
            })}
        </div>
    )
}
export default Courses;