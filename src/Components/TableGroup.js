import React from 'react';
import {Table} from 'reactstrap';
import './tableStyle.css';
const TableGroup =()=>{
    return (
        <div>
        <h3>★ New Operations Personnel Training</h3>

        <Table className="Table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Task</th>
              <th scope="col">Task Number</th>
              <th scope="col">Lesson</th>
              <th scope="col">Test</th>
              <th scope="col">Reference</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Maintain DA Form 1594 (Daily Staff Journal)</td>
              <td>011-141-1015</td>
              <td href="#">Lesson </td>
              <td href="#">Test</td>
              <td>AR 220-15 , DA Form 1594</td>
            </tr>
            <tr>
              <td>Maintain Notice To Airmen (NOTAM)</td>
              <td>011-141-1023</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>TC 3-04.16 , NOTAM Website</td>
            </tr>
            <tr>
              <td>Maintian Flight Information Display	</td>
              <td>011-141-0002</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Process DA Form 5484 (Mission Scheduler/Brief)</td>
              <td>011-141-1012</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Decode METAR Reports</td>
              <td>011-141-0025</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Initiate Pre-Accident Plan</td>
              <td>011-141-0113</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>--</td>
            </tr>
            <tr>
              <td>Conduct Shift Change Brief</td>
              <td>011-141-3060</td>
              <td href="#">Lesson</td>
              <td href="#">Test</td>
              <td>--</td>
            </tr>
          </tbody>
        </Table>

        <h3>Flight Operations Training</h3>
        <Table className="Table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">Role / Permissions</th>
              <th scope="col">Lesson</th>

            </tr>
          </thead>
          <tbody>

          <tr>
          <td>CAFRS Unit Administrator</td>
          <td href="../01 Lessons/00 CAFRS/CAFRS Unit Admin.pdf">Lesson</td>
        </tr>
          <tr>
          <td>Commander</td>
          <td  href="../01 Lessons/00 CAFRS/Commander.pdf">Lesson</td>
        </tr>
          <tr>
          <td>Flight Operations OIC</td>
          <td href="~/01 Lessons/00 CAFRS/Flight Operations OIC.pdf">Lesson</td>
        </tr>
          <tr>
          <td>*Flight Operations Personnel</td>
          <td href="../01 Lessons/00 CAFRS/Flight Operations Personnel.pdf">Lesson</td>
        </tr>
          <tr>
          <td>Instructor / Examiner</td>
          <td href="../01 Lessons/00 CAFRS/InstructorExaminer.pdf">Lesson</td>
        </tr>
          <tr>
          <td>Safety Personnel</td>
          <td href="../01 Lessons/00 CAFRS/Safety Personnel.pdf">Lesson</td>
        </tr>
        </tbody>
        </Table>

        <h3>Centralized Aviation Flight Records System (CAFRS)</h3>
      <Table className="Table">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Task</th>
            <th scope="col">Task Number</th>
            <th scope="col">Lesson</th>
            <th scope="col">Test</th>
            <th scope="col">Reference</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          <td>Maintain Aircraft Manifests</td>
          <td>011-141-1013</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Process DD Form 175 (Military Flight Plan)</td>
          <td>011-141-1049</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Initiate Overdue Aircraft Procedures</td>
          <td>011-141-1046</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Use Map Overlay</td>
          <td>071-329-1019</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Maintain DOD Flight Information Publication (FLIPS) Account</td>
          <td>011-141-2043</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Relocate a Tactical Operations Center (TOC)</td>
          <td>011-141-4048</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
        </tbody>
      </Table>
      
      <h3>Individual Flight Record Folder Training</h3>
      <Table className="Table">
            <thead className="thead-dark">
          <tr>
            <th scope="col">Task</th>
            <th scope="col">Task Number</th>
            <th scope="col">Lesson</th>
            <th scope="col">Test</th>
            <th scope="col">Reference</th>
          </tr>
        </thead>
        <tbody>
          <tr>
          <td>Inspect Maintenance of DA Form 3513 Individual Flight Record Folder (IFRF)</td>
          <td>011-141-3053</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Determine Eligibility for Flight Status</td>
          <td>011-141-2029</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Process Individual Flight Records</td>
          <td>011-141-1072</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Process Flight Orders for Flying Status</td>
          <td>011-141-1048</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Process Request for Aviation Badges</td>
          <td>011-141-3054</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Maintain DA Form 3513 Individual Flight Record Folders (IFRF)</td>
          <td>011-141-0105</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Inspection Preperation of DA Form 4730 (Certificate for Performace of Hazardus Duty)</td>
          <td>011-141-3055</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
        </tbody>
      </Table>
      
      <h3>Communications Training</h3>
      <Table className="Table">
        <thead className="thead-dark">
        <tr>
          <th>Task</th>
          <th>Task Number</th> 
          <th>Lesson</th>
          <th>Test</th>
          <th>References</th>
        </tr>
      </thead>
      <tbody>
          <tr>
          <td>Install Antenna Group OE-254/GRC (Team Method)</td>
          <td>113-596-1068</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Operate SINCGARS Single-Channel (SC)</td>
          <td>113-587-2070</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Operate Simple Key Loader (SKL) AN/PYQ-10</td>
          <td>113-609-2006</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Operate SINCGARS Frequency Hopping (FH) (Net Members)</td>
          <td>113-578-2071</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Load the Combat Survivor Evador Locator (CSEL) Radio</td>
          <td>011-141-1075</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
        </tbody>
      </Table>
 
      <h3>S3 Operations Training</h3>
      <Table className="Table">
        <thead className="thead-dark">
        <tr>
          <th>Task</th>
          <th>Task Number</th> 
          <th>Lesson</th>
          <th>Test</th>
          <th>References</th>
        </tr>
        </thead>
        <tbody>
          <tr>
          <td>Process Information from OPORDS, WARNOS, and FRAGOS</td>
          <td>011-141-1067</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Schedule Unit's Training Events</td>
          <td>011-141-1077</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Process Unit's Travel Arrangements Using Defense Travel System (DTS) (S3 Only)</td>
          <td>011-141-1076</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="../00 References/Joint Federal Travel Regulation.pdf">Joint Federal Travel Regulation</td>
        </tr>
          <tr>
          <td>Process Course Enrollments (S3 Only)</td>
          <td>011-141-1078</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Coordinate Training Ammunition Requirements for a Unit (S3 Only)</td>
          <td>011-141-2002</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Coordinate Training Site Requirements for a Unit (S3 Only)</td>
          <td>011-141-2001</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Participate in the Military Decision Making Process</td>
          <td>011-141-4401</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Implement Battle Rhythm</td>
          <td>011-141-3061</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
          <tr>
          <td>Conduct Troop Leading Procedures</td>
          <td>011-141-4046</td>
          <td href="#">Lesson</td>
          <td href="#">Test</td>
          <td href="#">--</td>
        </tr>
        </tbody>
      </Table>

                
                </div>
 
    )
}
export default TableGroup;