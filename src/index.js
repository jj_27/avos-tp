import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as firebase from 'firebase';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import CustomNavbar from './CustomNavbar';
import CourseOfferings from './Components/Courses/CourseOfferings';



 // Your web app's Firebase configuration
 var firebaseConfig = {
  apiKey: "AIzaSyCxnGJR0tSZd-jzcvsYQQb-IxmFX5Dk2xg",
  authDomain: "avos-tp.firebaseapp.com",
  databaseURL: "https://avos-tp.firebaseio.com",
  projectId: "avos-tp",
  storageBucket: "avos-tp.appspot.com",
  messagingSenderId: "918556433600",
  appId: "1:918556433600:web:f193b6ece9a7b337d9e125"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <React.StrictMode>
      <CustomNavbar/>
  <Router>
    <Route exact path="/" component={App}/>
    <Route exact path="/Courses" component={CourseOfferings}/>
  </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
