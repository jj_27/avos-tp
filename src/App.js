import React from 'react';
import './App.css';
import TableGroup from './Components/TableGroup';
import Courses from './Components/Courses/CourseOfferings';
function App() {
  return (
    <div className="App">
   
      <header className="App-header">
       Aviation Operations Specialist Training Programdd
       <div className="c">
         <div className="c-body">
           <blockquote className="bq">
             <p>"High Above the Best"</p>
           </blockquote>
         </div>
       </div>
      </header>
        <TableGroup/>
        
    </div>
  );
}

export default App;
